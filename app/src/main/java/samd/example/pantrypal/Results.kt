package samd.example.pantrypal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.text.method.ScrollingMovementMethod
import android.widget.TextView

class Results : AppCompatActivity()  {

    private lateinit var textView : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.results)
        textView = findViewById(R.id.results)
        textView.movementMethod = ScrollingMovementMethod()
        val recipes = intent.getStringArrayListExtra("recipes")
        textView.movementMethod = LinkMovementMethod.getInstance();
        if (recipes != null) {
            for (i in recipes) {
                val name = i.substringBefore("http")
                val link = i.substringAfterLast(" ")
                val url = "<a href='$link'>$link</a>"
                textView.append(name)
                textView.append("\n")
                textView.append(Html.fromHtml(url))
                textView.append("\n")
                textView.append("\n")
            }
            if (recipes.size == 0) textView.append("No recipes found")
        }
    }

    fun back(view: android.view.View) {
        finish()
    }
}