package samd.example.pantrypal

import android.provider.BaseColumns

class DBContract {
    class RecipeEntry : BaseColumns {
        companion object {
            val TABLE_NAME = "recipes"
            val COLUMN_SOURCE = "source"
            val COLUMN_NAME = "name"
            val COLUMN_URL = "url"
            val COLUMN_INGREDIENTS = "ingredients"
        }
    }


}