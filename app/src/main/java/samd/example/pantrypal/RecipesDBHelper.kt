package samd.example.pantrypal

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.database.Cursor
import android.database.DatabaseUtils
import android.database.sqlite.SQLiteException
import androidx.appcompat.app.AlertDialog

class RecipesDBHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES)
        println("Table Created")
}

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        val db = writableDatabase
        //db.execSQL(SQL_DELETE_ENTRIES)
        //onCreate(db)
    }

    fun dbExists() {

    }

    fun addRecipe(array : ArrayList<String>) {

        val db = writableDatabase
        val entry = ContentValues()

        entry.put(DBContract.RecipeEntry.COLUMN_SOURCE, array[0])
        entry.put(DBContract.RecipeEntry.COLUMN_NAME, array[1])
        entry.put(DBContract.RecipeEntry.COLUMN_URL, array[2])
        entry.put(DBContract.RecipeEntry.COLUMN_INGREDIENTS, array[3])

        println("Adding Recipe: " + array[0] + " " + array[1] + " " + array[2] + " " + array[3])
        db.insert(DBContract.RecipeEntry.TABLE_NAME, null, entry)

    }


    @SuppressLint("Range")
    fun getRecipes(ingredients_array: ArrayList<String>) : ArrayList<String> {
        val recipes = ArrayList<ArrayList<String>>()
        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery("select * from " + DBContract.RecipeEntry.TABLE_NAME, null)
        } catch (e: SQLiteException) {
            println("Database error")
        }

        var source: String
        var name: String
        var URL: String
        var ingredients: String

        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                source = cursor.getString(cursor.getColumnIndex(DBContract.RecipeEntry.COLUMN_SOURCE))
                name = cursor.getString(cursor.getColumnIndex(DBContract.RecipeEntry.COLUMN_NAME))
                URL = cursor.getString(cursor.getColumnIndex(DBContract.RecipeEntry.COLUMN_URL))
                ingredients = cursor.getString(cursor.getColumnIndex(DBContract.RecipeEntry.COLUMN_INGREDIENTS))

                val r = ArrayList<String>()
                r.add(source)
                r.add(name)
                r.add(URL)
                r.add(ingredients)
                println(r)
                recipes.add(r)
                cursor.moveToNext()
            }
        }
        var list = checkList(recipes, ingredients_array)
        return list
    }

    fun checkList(recipes : ArrayList<ArrayList<String>>, ingredients_array : ArrayList<String>): ArrayList<String> {

            var final_recipes = ArrayList<String>()
            println("Ingredients On Hand: $ingredients_array")
            for (i in recipes) {
                var added = false
                val newlist: MutableList<String> = i[3].split(", ") as MutableList<String>
                println("Recipe: " + i[1] +  " Ingredients: " + i[3])
                for (j in ingredients_array) {
                    println("Current Ingredient: $j")
                    if (newlist.contains(j)) {
                        println("Found " + j + " in ingredient list: $newlist")
                        println("Removing " + j)
                        if (newlist.size == 1) {
                            added = true
                            val sb = StringBuilder()
                            sb.append(i[1] + " " + i[2])
                            val rec = sb.toString()
                            final_recipes.add(rec)

                        } else {
                            newlist.remove(j)
                            println(newlist)
                        }
                    }

                    else println("Can't find " + j + " in recipe " + i[1])
                }
                if (newlist.size == 1) {
                    val sb = StringBuilder()
                    sb.append(i[1] + " - Missing Ingredient: " + newlist[0] + " " + i[2])
                    val rec = sb.toString()
                    if (!added) { final_recipes.add(rec) }
                }
            }
            println("Possible recipes: $final_recipes")
            return final_recipes
    }

    fun returnTableSize() {
        val db = readableDatabase
        val count = DatabaseUtils.queryNumEntries(db, DBContract.RecipeEntry.TABLE_NAME)
        println("Current recipes count: " + count)
    }


    fun resetTable() {
        val db = writableDatabase
        db.execSQL(SQL_CLEAR_ENTRIES)
    }

    companion object {
        // If you change the database schema, you must increment the database version.
        val DATABASE_VERSION = 1
        val DATABASE_NAME = "recipes.db"

        private val SQL_CREATE_ENTRIES =
            "CREATE TABLE " + DBContract.RecipeEntry.TABLE_NAME + " (" +
                    DBContract.RecipeEntry.COLUMN_SOURCE + " TEXT," +
                    DBContract.RecipeEntry.COLUMN_NAME + " TEXT," +
                    DBContract.RecipeEntry.COLUMN_URL + " TEXT," +
                    DBContract.RecipeEntry.COLUMN_INGREDIENTS + " TEXT)"

        private val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + DBContract.RecipeEntry.TABLE_NAME

        private val SQL_CLEAR_ENTRIES = "DELETE FROM " + DBContract.RecipeEntry.TABLE_NAME
    }

}