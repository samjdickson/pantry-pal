package samd.example.pantrypal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import android.content.DialogInterface
import android.content.DialogInterface.OnMultiChoiceClickListener
import android.content.Intent
import android.text.method.ScrollingMovementMethod
import android.widget.TextView
import org.jsoup.Jsoup
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var recipesDBHelper : RecipesDBHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recipesDBHelper = RecipesDBHelper(this)
    }

    private val searchIngredients = ArrayList<String>()
    var ingredients = arrayOf<String>()
    var checkedItems = BooleanArray(160)
    var checkedSites = booleanArrayOf(true, true)
    var sitesSelected = ArrayList<String>()

    fun ingredientsPopup(view: android.view.View) {

        val popup = AlertDialog.Builder(this)
        popup.setTitle("Select ingredients")
        searchIngredients.clear()
        ingredients = resources.getStringArray(R.array.ingredients)

        popup.setMultiChoiceItems(ingredients, checkedItems,
            OnMultiChoiceClickListener { dialog, which, isChecked ->
                checkedItems[which] = isChecked;
            })

        popup.setPositiveButton("OK",
            DialogInterface.OnClickListener { dialog, which ->
                for(e in checkedItems.withIndex()){
                    if (e.value) {
                        searchIngredients.add(ingredients[e.index])
                    }
                }
                val new: TextView = findViewById(R.id.ingredients_selected)
                new.movementMethod = ScrollingMovementMethod()
                new.setText("")
                for (i in searchIngredients) {
                    new.append(i)
                    new.append("\n")
                }
            })

        popup.setNegativeButton("Cancel", null)

        val dialog: AlertDialog = popup.create()
        dialog.show()
    }

    //Pop-Up for database search settings
    fun dbPopUp(view: android.view.View) {
        val popup = AlertDialog.Builder(this)
        popup.setTitle("Search from:")
        sitesSelected.clear()
        var sites = arrayOf("BBC", "Recipe Tin Eats")
        var sitesSelected = ArrayList<String>()

        popup.setMultiChoiceItems(sites, checkedSites,
            OnMultiChoiceClickListener { dialog, which, isChecked ->
                checkedSites[which] = isChecked;
            })

        popup.setPositiveButton("Update",
            DialogInterface.OnClickListener { dialog, which ->
                for(e in checkedSites.withIndex()){
                    if (e.value) {
                        sitesSelected.add(sites[e.index])
                    }
                }
                clearDB(sitesSelected)
            })

        popup.setNegativeButton("Cancel", null)
        val dialog: AlertDialog = popup.create()
        dialog.show()
    }



    fun ingredientsClear(view: android.view.View) {
        val ingList: TextView = findViewById(R.id.ingredients_selected)
        ingList.setText("")
        searchIngredients.clear()
        clearChecked()
    }

    fun clearChecked() {
        Arrays.fill(checkedItems, false)
    }

    fun search(view: android.view.View) {
        println("Searching for recipes using $searchIngredients")
        val recipes = recipesDBHelper.getRecipes(searchIngredients)

        //resultsPopUp(recipes)
        searchPage(recipes)
    }

    fun searchPage(recipes: ArrayList<String>) {
        val intent = Intent(this, Results::class.java)
        intent.putStringArrayListExtra("recipes", recipes)
        startActivity(intent)
    }

//Recipe Scraper Scripts

    //Scrape BBCGoodFood
    fun scrapeBBC () {
        for (i in 1..5) {
            Thread (Runnable {
                try {
                    var bbc = "https://www.bbcgoodfood.com/search/recipes/page/"+i+"/?q=Most+popular+recipes&sort=-popular&rating=4-star";
                    println(bbc)
                    val site = Jsoup.connect(bbc).get()
                    val elements = site.select("a.standard-card-new__article-title")

                    for (e in elements) {
                        var url = "https://www.bbcgoodfood.com" + e.attr("href")
                        getBBCRecipes(e.text(), url)
                    }
                } catch (e: IOException) {
                    println("Connection Error")
                    runOnUiThread(Runnable {
                        Toast.makeText(
                            this,
                            "Connection Error: Please check your internet connection and try again",
                            Toast.LENGTH_SHORT
                        ).show()
                    })
                }
            }).start()
        }
    }

    fun getBBCRecipes (name: String, recipeURL: String) {
        Thread (Runnable {
            try {
                val site = Jsoup.connect(recipeURL).get()
                val ings = site.select("li.pb-xxs.pt-xxs.list-item.list-item--separator")
                val list = ArrayList<String>()
                for (i in ings) {
                    list.add(i.text())
                }
                var array = ArrayList<String>()
                var ingredients = list.joinToString()
                array.add("BBCGoodFood")
                array.add(name)
                array.add(recipeURL)
                array.add(findIngredients(ingredients))
                if (name != "Roast chestnuts") {
                    recipesDBHelper.addRecipe(array)
                }
            } catch (e: IOException) {
                println("Connection Error")
                runOnUiThread(Runnable {
                    Toast.makeText(
                        this,
                        "Connection Error: Please check your internet connection and try again",
                        Toast.LENGTH_SHORT
                    ).show()
                })
            }
        }).start()
    }

    fun scrapeTinEats() {
        for (i in 1..5)
        Thread (Runnable {
            try {
                var taste = "https://www.recipetineats.com/category/most-popular/page/" + i
                val site = Jsoup.connect(taste).get()
                val elements = site.select("a.entry-title-link")
                for (e in elements) {
                    getRTERecipes(e.text(), e.attr("href"))
                }

            } catch (e: IOException) {
                println("Connection Error")
                runOnUiThread(Runnable {
                    Toast.makeText(
                        this,
                        "Connection Error: Please check your internet connection and try again",
                        Toast.LENGTH_SHORT
                    ).show()
                })
            }
        }).start()
    }

    fun getRTERecipes(name: String, recipeURL: String) {
        Thread (Runnable {
            try {
                val site = Jsoup.connect(recipeURL).get()
                val ings = site.select("span.wprm-recipe-ingredient-name")
                val list = ArrayList<String>()
                for (i in ings) {
                    list.add(i.text())
                }
                var array = ArrayList<String>()
                var ingredients = list.joinToString()
                array.add("RecipeTinEats")
                array.add(name)
                array.add(recipeURL)
                array.add(findIngredients(ingredients))
                if (name != "Standby Salad Dressing Recipes") {
                    recipesDBHelper.addRecipe(array)
                }
            } catch (e: IOException) {
                println("Connection Error")
                runOnUiThread(Runnable {
                    Toast.makeText(
                        this,
                        "Connection Error: Please check your internet connection and try again",
                        Toast.LENGTH_SHORT
                    ).show()
                })
            }
        }).start()
    }

//Ingredient sorter
    fun findIngredients(list: String) : String {
        ingredients = resources.getStringArray(R.array.ingredients)
        var ings = ArrayList<String>()
        for (i in ingredients) {
            if (list.contains(i, ignoreCase = true)) {
                ings.add(i)
            }
        }
        var ingString = ings.joinToString()
        return ingString
    }

//Database Admin Functions
    fun clearDB(sites : ArrayList<String>) {
        recipesDBHelper.resetTable()
        if (sites.contains("BBC")) {
            scrapeBBC()
        }
        if (sites.contains("Recipe Tin Eats")) {
            scrapeTinEats()
        }
    }

}